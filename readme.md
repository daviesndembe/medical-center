# Medical Center


## System functions
-Maintain Database
-Patient Registration
-Laboratory report Generation
-Security Management


## Clients of the System
-Receptionist
-Doctors
-Laboratory attendants
-Medical store attendant
-System Administrator


## Functional Requirements

### Doctor
(1)Prescribe Medicine
-The Doctor will prescribe the medicine to the patient which will be stored in the Patient record Update Form.

(2)Updates Patients checkup data
-This activity will let doctor to update the patients checkup information in the patient record.

(3)See Laboratory reports
-The doctor will see the reports about the patients tests and makes analysis from the report

### Receptionist
(1)Doctors appointment scheduling
-The receptionist will keep the record patients appointments the doctors

(2)Inquiry of patient
-The Receptionist will handle various Inquiries about the patient registration.

(3)Find History of Patient Inquired
-This activity will help Receptionist to find the history of the patients who were inquired for their Registration Information.

### System Administrator
(1)Inspect Performance
-The System Administrator will be responsible for the overall inspection of the system

###Medical Store Attendant
(1)Keeps record of medicine stock
-The medical store attendant will keeps the record of the acquiring of medicines of whether purchase or donations, and also that of medicine stock in the medicine store.
-The medical store will be updated when medicine is purchased, donated or when prescribed to the patients.

### Laboratory Attendant
(1)Updates Tests Data
-The Lab attendant will update the patients tests data and the result will be used in generating reports.
-The generated reports will be related to the test done, doctor remarks and precautions.


##Non-Functional Requirements
(1)Maintainability
-The system should be developed in such a manner that its functionality can be enhanced to support further development in the system.

(2)Security
-Security is provided in system a way that the different users will not be able able to access everything in the system. The database will no be within everyone reach.

(3)Reliability
-The system shall be reliable. If the server crashes, the data will not be lost because a backup will be maintained.


##User Characteristics