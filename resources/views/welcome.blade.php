@extends('layouts.app')

@section('content')

<div class="jumbotron">
    <div class="container">
        <h1>{{ config('app.name') }}</h1>
        <p>
            Network-space market denim Kowloon wonton soup shoes. Free-market dome camera augmented reality assault render-farm bomb table into boat BASE jump human digital semiotics. Military-grade savant convenience store concrete digital kanji marketing neon render-farm boy math-sprawl katana sign girl saturation point weathered. Concrete stimulate shanty town realism office dome math-disposable augmented reality faded hacker sunglasses. 
        </p>
        <p>
            <a class="btn btn-primary btn-lg">Learn more</a>
        </p>
    </div>
</div>

@endsection
