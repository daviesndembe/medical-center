@extends('layouts.app')

@section('page_title', 'Staff - ')

@section('content')
    <div class="jumbotron">
        <div class="container">
            <h1>Staff</h1>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#all-departments" aria-controls="all-departments" role="tab" data-toggle="tab">All
                                Departments</a>
                        </li>
                        @if($departments->count() > 0)
                            @foreach($departments as $department)
                                <li role="presentation">
                                    <a href="#{{ $department->id }}" aria-controls="{{ $department->id }}" role="tab"
                                       data-toggle="tab">{{ $department->display_name }}</a>
                                </li>
                            @endforeach
                        @endif
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="all-departments">
                            <h2>All Departments</h2>
                            @foreach($doctors->chunk(4) as $doctorsSet)
                                <div class="row">
                                    @foreach($doctorsSet as $doctor)
                                        @include('staff._card', ['doctor' => $doctor])
                                    @endforeach
                                    <div class="clearfix"></div>
                                </div>
                            @endforeach

                        </div>
                        @if($departments->count() > 0)
                            @foreach($departments as $department)
                                <div role="tabpanel" class="tab-pane" id="{{ $department->id }}">
                                    <h2>{{ $department->display_name }}</h2>
                                    @if($department->doctors->count() > 0)
                                        @foreach($department->doctors->chunk(4) as $doctorsSet)
                                            <div class="row">
                                                @foreach($doctorsSet as $doctor)
                                                    @include('staff._card', ['doctor' => $doctor])
                                                @endforeach
                                                <div class="clearfix"></div>
                                            </div>
                                        @endforeach
                                    @else
                                        <h3 class="text-center">No doctor</h3>
                                    @endif
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
