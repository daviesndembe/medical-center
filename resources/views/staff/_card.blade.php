<div class="col-sm-3">
	<div class="thumbnail">
		<img data-src="#" alt="">
		<div class="caption">
			<h3>{{ $doctor->name }}</h3>
			<p>
				{{  str_limit($doctor->description, 100) }}
			</p>
			<p>
				@include('staff._social', ['doctor' => $doctor])
			</p>
			<p>
				<a href="{{ url('staff/' . $doctor->slug) }}" class="btn btn-primary">Read More</a>
			</p>
		</div>
	</div>
</div>