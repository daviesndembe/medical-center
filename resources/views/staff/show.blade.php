@extends('layouts.app')

@section('page_title', $doctor->name . ' - ')

@section('content')
<div class="jumbotron">
    <div class="container">
        <h1>{{ $doctor->name }}</h1>
        <p>
            <a class="btn btn-primary btn-lg">Make Appointment</a>
        </p>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-sm-3">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-xs-12">
                                <a href="#" class="thumbnail">
                                    <img src="#" alt="">
                                </a>
                            </div>
                            <h2>{{ $doctor->name }}</h2>
                            @if($doctor->departments->count() > 0)
                                @foreach($doctor->departments as $department)
                                    <a href="{{ url('departments/' . $department->name) }}">{{ $department->display_name }}</a>
                                @endforeach
                            @endif
                            <dl>
                              <dt>Speciality</dt>
                              <dd>
                                {{ isset($doctor->speciality) ? $doctor->speciality->display_name : '-' }}
                              </dd>
                              <dt>Education</dt>
                              <dd>
                                {{ isset($doctor->education) ? $doctor->education->display_name : '-' }}
                               </dd>
                              <dt>Work Days</dt>
                              <dd></dd>
                            </dl> 
                            <hr>
                            @include('staff._social', ['doctor' => $doctor])
                        </div>
                    </div>                    
                </div>
                <div class="col-sm-9">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="page-header">
                              <h2>{{ $doctor->name }}</h2>
                            </div>
                            <article>
                                {{ $doctor->description }}
                            </article>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h2>Related Staff</h2>
        </div>
    </div>
</div>
@endsection
