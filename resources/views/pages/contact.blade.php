@extends('layouts.app')

@section('content')

    <div class="jumbotron">
        <div class="container">
            <h1>Contact Us</h1>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <h2 class="text-center">We really appreciate your feedback</h2>

                {!! Form::open(['url' => 'contact', 'class' => 'form-horizontal']) !!}

                <div class="form-group">
                    {{ Form::label('name', 'Name', ['class' => 'col-sm-2 control-label']) }}
                    <div class="col-sm-4">
                        {{ Form::text('name', null, ['class' => 'form-control']) }}
                    </div>
                    {{ Form::label('email', 'Email', ['class' => 'col-sm-1 control-label']) }}
                    <div class="col-sm-5">
                        {{ Form::text('email', null, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('message', 'Message', ['class' => 'col-sm-2 control-label']) }}
                    <div class="col-sm-10">
                        {{ Form::textarea('message', null, ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button type="submit" class="btn btn-primary">Send</button>
                    </div>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection
