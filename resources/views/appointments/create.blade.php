@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Make an appointment</div>

                    <div class="panel-body">

                        <div class="row">
                            <div class="col-sm-9">
                                {!! Form::open(['url' => 'appointments', 'class' => 'form-horizontal']) !!}

                                @include('appointments._form')

                                <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-2">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>

                                {!! Form::close() !!}
                            </div>
                            <div class="col-sm-3">
                                <h3>Best practices</h3>
                                <p>
                                    Engine bicycle jeans beef noodles denim sentient smart-computer. Bridge soul-delay
                                    garage drone market augmented reality artisanal San Francisco. Artisanal tank-traps
                                    numinous range-rover 3D-printed crypto-computer bicycle weathered convenience store
                                    papier-mache denim bomb man.
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
