<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    {!! Form::label('name', 'Name', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        @if($errors->has('name'))
            <p class="help-block">
                {{ $errors->first('name') }}
            </p>
        @endif
    </div>
</div>
<div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
    {!! Form::label('phone', 'Phone', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('phone', null, ['class' => 'form-control']) !!}
        @if($errors->has('phone'))
            <p class="help-block">
                {{ $errors->first('phone') }}
            </p>
        @endif
    </div>
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
    {!! Form::label('email', 'Email', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
        @if($errors->has('email'))
            <p class="help-block">
                {{ $errors->first('email') }}
            </p>
        @endif
    </div>
</div>
<div class="form-group {{ $errors->has('appointment_date') ? 'has-error' : '' }}">
    {!! Form::label('appointment_date', 'Appointment Date', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::date('appointment_date', null, ['class' => 'form-control']) !!}
        @if($errors->has('appointment_date'))
            <p class="help-block">
                {{ $errors->first('appointment_date') }}
            </p>
        @endif
    </div>
</div>
<div class="form-group {{ $errors->has('message') ? 'has-error' : '' }}">
    {!! Form::label('message', 'Message', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('message', null, ['class' => 'form-control']) !!}
        @if($errors->has('message'))
            <p class="help-block">
                {{ $errors->first('message') }}
            </p>
        @endif
    </div>
</div>