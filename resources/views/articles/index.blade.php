@extends('layouts.app')

@section('page_title', 'News - ')

@section('content')

    <div class="jumbotron">
        <div class="container">
            <h1>News</h1>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-9">

                @if($articles->count() > 0)

                    @foreach($articles as $article)
                        <article>
                            <div class="row">
                                <div class="col-sm-2">
                                    {{ $article->created_at }}
                                </div>
                                <div class="col-sm-10">
                                    <h2>{{ $article->title }}</h2>
                                    <div class="content">
                                        {!! str_limit($article->content, 200) !!}
                                    </div>
                                    <a href="{{ url('news/' . $article->slug) }}" class="btn btn-primary">Read More</a>
                                    <hr>
                                </div>
                            </div>
                        </article>
                    @endforeach

                @else

                @endif

            </div>
            <div class="col-md-3">

            </div>
        </div>
    </div>
@endsection
