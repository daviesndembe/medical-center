<div class="form-group">
    {{ Form::label('title', 'Title', ['class' => 'col-sm-2 control-label']) }}
    <div class="col-sm-10">
        {{ Form::text('title', null, ['class' => 'form-control']) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('content', 'Content', ['class' => 'col-sm-2 control-label']) }}
    <div class="col-sm-10">
        {{ Form::textarea('content', null, ['class' => 'form-control']) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('image', 'Image', ['class' => 'col-sm-2 control-label']) }}
    <div class="col-sm-10">
        {{ Form::file('image') }}
    </div>
</div>

{{ Form::hidden('user_id', Auth::user()->id) }}