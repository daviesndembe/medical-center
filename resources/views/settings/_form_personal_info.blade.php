<div class="form-group">
	{!! Form::label('name', 'Name') !!}
	{!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('bio', 'Bio') !!}
	{!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => '5']) !!}
</div>
<div class="form-group">
	{!! Form::label('email', 'Email') !!}
	{!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('slug', 'Username') !!}
	{!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => str_slug($user->name, '-')]) !!}
</div>