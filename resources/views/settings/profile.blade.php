@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    {!! Form::model($user, ['url' => ['users', $user->id]]) !!}

                        {{ method_field('patch') }}

                        @include('settings/_form_personal_info')

                        <button type="submit" name="update_personal_info_button" class="btn btn-primary">Update</button>

                    {!! Form::close() !!}
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Profession</div>

                <div class="panel-body">
                    {!! Form::model($user, ['url' => ['users', $user->id]]) !!}

                        @include('settings/_form_profession_info')

                        <button type="submit" name="update_profession_info_button" class="btn btn-primary">Update</button>

                    {!! Form::close() !!}
                </div>
            </div>  

            <div class="panel panel-default">
                <div class="panel-heading">Social</div>

                <div class="panel-body">
                    {!! Form::model($user, ['url' => ['users', $user->id]]) !!}

                        {{ method_field('patch') }}

                        @include('settings/_form_social_info')

                        <button type="submit" name="update_social_info_button" class="btn btn-primary">Update</button>

                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
