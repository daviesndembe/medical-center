<div class="form-group">
	{!! Form::label('speciality', 'Speciality') !!}
	{!! Form::text('speciality_id', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('education', 'Education') !!}
	{!! Form::text('education', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('work_days', 'Work Days') !!}
	{!! Form::text('work_days', null, ['class' => 'form-control']) !!}
</div>