<div class="form-group">
	{!! Form::label('twitter', 'Twitter') !!}
	{!! Form::text('twitter', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('facebook', 'Facebook') !!}
	{!! Form::text('facebook', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('linkedin', 'LinkedIn') !!}
	{!! Form::text('linkedin', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('skype', 'Skype') !!}
	{!! Form::text('skype', null, ['class' => 'form-control']) !!}
</div>