@extends('layouts.app')

@section('page_title', 'Services - ')

@section('content')

    <div class="jumbotron">
        <div class="container">
            <h1>Services</h1>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @if($services->count() > 0)
                    @foreach($services->chunk(2) as $serviceSet)
                        <div class="row">
                            @foreach($serviceSet as $service)
                                <div class="col-sm-6">
                                    <div class="thumbnail">
                                        <img data-src="#" alt="">
                                        <div class="caption">
                                            <h3>{{ $service->display_name }}</h3>
                                            <p>
                                                {{ str_limit($service->description, 200) }}
                                            </p>
                                            <p>
                                                <a href="{{ url('services/' . $service->name) }}" class="btn btn-primary">Read More</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                @else
                    <h2 class="text-center">No service found</h2>
                @endif

            </div>
        </div>
    </div>
@endsection
