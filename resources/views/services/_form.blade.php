<div class="form-group">
    {{ Form::label('display_name', 'Name', ['class' => 'col-sm-2 control-label']) }}
    <div class="col-sm-10">
        {{ Form::text('display_name', null, ['class' => 'form-control']) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('description', 'Description', ['class' => 'col-sm-2 control-label']) }}
    <div class="col-sm-10">
        {{ Form::textarea('description', null, ['class' => 'form-control']) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('image', 'Image', ['class' => 'col-sm-2 control-label']) }}
    <div class="col-sm-10">
        {{ Form::file('image') }}
    </div>
</div>
