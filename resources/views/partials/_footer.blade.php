<footer>
	<p class="text-center">
		Copyright &copy; {{ date('Y') }} {{ config('app.name', 'Medical Center') }} All Rights Reserved
	</p>
</footer>