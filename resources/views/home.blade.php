@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#staffs" aria-controls="staffs" role="tab" data-toggle="tab">Staff</a>
                                </li>
                                <li role="presentation">
                                    <a href="#articles" aria-controls="articles" role="tab"
                                       data-toggle="tab">News</a>
                                </li>
                                <li role="presentation">
                                    <a href="#services" aria-controls="services" role="tab"
                                       data-toggle="tab">Services</a>
                                </li>
                                <li role="presentation">
                                    <a href="#photos" aria-controls="photos" role="tab" data-toggle="tab">Photos</a>
                                </li>
                                <li role="presentation">
                                    <a href="#videos" aria-controls="videos" role="tab" data-toggle="tab">Videos</a>
                                </li>
                                <li role="presentation">
                                    <a href="#appointments" aria-controls="appointments" role="tab" data-toggle="tab">Appointments</a>
                                </li>
                                <li role="presentation">
                                    <a href="#feedbacks" aria-controls="feedbacks" role="tab" data-toggle="tab">Feedbacks</a>
                                </li>
                                <li role="presentation">
                                    <a href="#specialities" aria-controls="specialities" role="tab" data-toggle="tab">Specialities</a>
                                </li>
                                <li role="presentation">
                                    <a href="#educational-levels" aria-controls="educational-levels" role="tab" data-toggle="tab">Educational Levels</a>
                                </li>
                                <li role="presentation">
                                    <a href="#work-days" aria-controls="work-days" role="tab" data-toggle="tab">Work Days</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="staffs">
                                    <h3>All Staff</h3>

                                    @if($staffs->count() > 0)
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead>
                                                <tr>
                                                    <th>SN</th>
                                                    <th>Name</th>
                                                    <th>Joined</th>
                                                    <th>Last Login</th>
                                                    <th colspan="2">Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php $i = 1;?>
                                                @foreach($staffs as $staff)
                                                    <tr>
                                                        <td>{{ $i++ }}</td>
                                                        <td>{{ $staff->name }}</td>
                                                        <td>{{ $staff->created_at }}</td>
                                                        <td>{{ $staff->updated_at }}</td>
                                                        <td>
                                                            <a href="{{ url('staff/' . $staff->slug . '/edit') }}">Edit</a>
                                                        </td>
                                                        <td><a href="{{ url('') }}">Delete</a></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    @else
                                        <h2 class="text-center">No Staff</h2>
                                    @endif

                                </div>
                                <div role="tabpanel" class="tab-pane" id="articles">
                                    <h3>News <span class="pull-right"><a href="{{ url('news/create') }}"
                                                                         class="btn btn-primary">Add New</a></span></h3>

                                    @if($articles->count() > 0)
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead>
                                                <tr>
                                                    <th>SN</th>
                                                    <th>Name</th>
                                                    <th>Added</th>
                                                    <th>Updated</th>
                                                    <th colspan="2">Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php $i = 1;?>
                                                @foreach($articles as $article)
                                                    <tr>
                                                        <td>{{ $i++ }}</td>
                                                        <td>{{ $article->title }}</td>
                                                        <td>{{ $article->created_at }}</td>
                                                        <td>{{ $article->updated_at }}</td>
                                                        <td>
                                                            <a href="{{ url('news/' . $article->slug . '/edit') }}">Edit</a>
                                                        </td>
                                                        <td><a href="{{ url('') }}">Delete</a></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    @else
                                        <h2 class="text-center">No Article</h2>
                                    @endif

                                </div>
                                <div role="tabpanel" class="tab-pane" id="services">
                                    <h3>Services <span class="pull-right"><a href="{{ url('services/create') }}" class="btn btn-primary">Add New</a></span></h3>

                                    @if($services->count() > 0)
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead>
                                                <tr>
                                                    <th>SN</th>
                                                    <th>Name</th>
                                                    <th>Added</th>
                                                    <th>Updated</th>
                                                    <th colspan="2">Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php $i = 1;?>
                                                @foreach($services as $service)
                                                    <tr>
                                                        <td>{{ $i++ }}</td>
                                                        <td>{{ $service->display_name }}</td>
                                                        <td>{{ $service->created_at }}</td>
                                                        <td>{{ $service->updated_at }}</td>
                                                        <td><a href="{{ url('services/' . $service->name . '/edit') }}">Edit</a>
                                                        </td>
                                                        <td><a href="{{ url('') }}">Delete</a></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    @else
                                        <h2 class="text-center">No Services</h2>
                                    @endif

                                </div>
                                <div role="tabpanel" class="tab-pane" id="photos">
                                    <h3>Images</h3>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="videos">
                                    <h3>Videos</h3>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="appointments">
                                    <h3>Appointments</h3>

                                    @if($appointments->count() > 0)
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead>
                                                <tr>
                                                    <th>SN</th>
                                                    <th>Name</th>
                                                    <th>Phone</th>
                                                    <th>Email</th>
                                                    <th>Message</th>
                                                    <th>Appointment Date</th>
                                                    <th>Added</th>
                                                    <th>Updated</th>
                                                    <th colspan="2">Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php $i = 1;?>
                                                @foreach($appointments as $appointment)
                                                    <tr>
                                                        <td>{{ $i++ }}</td>
                                                        <td>{{ $appointment->name }}</td>
                                                        <td>{{ $appointment->phone }}</td>
                                                        <td>{{ $appointment->email }}</td>
                                                        <td>{{ str_limit($appointment->message, 100) }}</td>
                                                        <td>{{ $appointment->appointment_date }}</td>
                                                        <td>{{ $appointment->created_at }}</td>
                                                        <td>{{ $appointment->updated_at }}</td>
                                                        <td>
                                                            <a href="{{ url('news/' . $appointment->slug . '/edit') }}">Edit</a>
                                                        </td>
                                                        <td><a href="{{ url('') }}">Delete</a></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    @else
                                        <h2 class="text-center">No Appointments</h2>
                                    @endif

                                </div>
                                <div role="tabpanel" class="tab-pane" id="feedbacks">
                                    <h3>Feedbacks</h3>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="specialities">
                                    <h3>Specialities</h3>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="educational-levels">
                                    <h3>Educational Levels</h3>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="work-days">
                                    <h3>Work Days</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
