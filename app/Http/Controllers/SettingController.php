<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

class SettingController extends Controller
{
    public function profile()
    {
    	$user = Auth::user();
    	return view('settings/profile', compact('user'));
    }

    public function update(Request $request, $id)
    {
    	return $request->all();
    }
}
