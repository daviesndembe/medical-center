<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\Department;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::all();
        $doctors = User::all();
        return view('staff.index', compact('departments', 'doctors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $doctor = User::whereSlug($slug)->first();
        return view('staff.show', compact('doctor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $user = User::whereSlug($slug)->first();
        return view('settings/profile', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(isset($_POST['update_personal_info_button'])){
            // update personal info
            $user = User::findOrFail($id);
            $user->name = $request->input('name');
            $user->description = $request->input('description');
            $user->email = $request->input('email');
            $user->slug = 'dr-' . str_slug($request->input('name'), '-');
            $user->save();

            return redirect('settings/profile');
        }

        if(isset($_POST['update_profession_info_button'])){
            // update profession info
        }

        if(isset($_POST['update_social_info_button'])){
            // update social info
            $user = User::findOrFail($id);
            $user->twitter = $request->input('twitter');
            $user->facebook = $request->input('facebook');
            $user->linkedin = $request->input('linkedin');
            $user->skype = $request->input('skype');
            $user->save();

            return redirect('settings/profile');            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
