<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Article;
use App\Service;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staffs = User::all();
        $articles = Article::all();
        $services = Service::all();
        $appointments = Appointment::all();
        return view('home', compact('staffs', 'articles', 'services', 'appointments'));
    }
}
