<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function departments()
    {
        return $this->belongsToMany('App\Department')->withTimestamps();
    }

    public function speciality()
    {
        return $this->belongsTo('App\Speciality');
    }   

    public function education()
    {
        return $this->belongsTo('App\Education');
    } 

    public function work_days()
    {
        return $this->belongsToMany('App\WorkDay')->withTimestamps();
    }

    public function articles()
    {
        return $this->hasMany('App\Article');
    }
}
