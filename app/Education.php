<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    protected $fillable = [
    	'display_name',
    	'name',
    	'description',
    ];

    public function doctors()
    {
    	return $this->hasMany('App\User');
    }
}
