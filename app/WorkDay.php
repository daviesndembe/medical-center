<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkDay extends Model
{
    protected $fillable = [
    	'display_name',
    	'name',
    	'description',
    ];

    public function doctors()
    {
    	return $this->belongsToMany('App\User')->withTimestamps();
    }    
}
