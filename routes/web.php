<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('departments', 'DepartmentController');

Route::resource('staff', 'UserController');

Route::get('settings/profile', 'SettingController@profile');

Route::patch('users/{id}', 'UserController@update');

Route::resource('news', 'ArticleController');

Route::resource('services', 'ServiceController');

Route::resource('appointments', 'AppointmentController');

Route::group(['namespace' => 'Gallery'], function () {
    Route::resource('gallery/images', 'ImageController');
    Route::resource('gallery/videos', 'VideoController');
});

Route::get('contact', 'PageController@contact');
